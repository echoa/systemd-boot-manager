# This is no longer a good option for Systemd-boot conversion/use and Dalto has created a superior method. DO NOT USE THIS and please use https://gitlab.com/dalto.8/eos-systemd-boot/ as this project will not be updated. While I tried to do some fun things with this converting Daltos old scripts for manjaro for use elsewhere he has better understanding of this and has provided a far superior method. Ill leave this here as a reference for possibly creating other scripts,etc.

NOTE: In its current state its not likely to handle multiboot and maybe other issues. This is an early state hack of work by dalto at best…there be dragons here

##### systemd-boot Manager for Arch

This projects attempts to add automation for users of systemd-boot with Arch.  It has 3 components.

##### 1. A shell script which provides the following functionality
  * Generate entries based on installed kernels and a series of options described below
  * Removes entries for kernels no longer installed
  * Allows user to select the default boot option
##### 2. A series of alpm hooks that are called to automate the process
##### 3. A configuration file to support a variety of use cases described below

### Usage
While the script is primarily intended to automated via the provided hooks, it can also be invoked manually as follows:
```
Usage: sdboot-manage [action]

Actions:
  gen     generates entries for systemd-boot based on installed kernels
  remove  removes orphaned systemd-boot entries
  default  Select default boot .conf
```

### Filesystem Support
The script original script by dalto was tested for basic functionality in the following root filesystem configurations:
* btrfs
* ext4
* f2fs
* zfs
* ext4 on luks
* ext4 on lvm on luks
* ext4 on luks on lvm

This "should" still be true but has not yet been tested, NTFS root is not supported and you shouldnt do that anyway

### Configuration
The configuration file is located at `/etc/sdboot-manage.conf`.  The displayed options are the defaults and it has the following structure.
```
# config file for sdboot-manage

# kernel options to be appended to the "options" line
#LINUX_OPTIONS=""
#LINUX_FALLBACK_OPTIONS=""

# when LINUX_USE_DEVICE_FOR_RESUME is set to "yes", the specified device will be used for hibernation
#LINUX_USE_DEVICE_FOR_RESUME=/dev/sda4
#LINUX_USE_DEVICE_FOR_RESUME=UUID=device_uuid

# when LINUX_USE_SWAP_FOR_RESUME is set to "yes", the first detected available swap device will be used for hibernation
# i.e. the "resume=UUID=swap_device" parameter would be appended to the kernel command line
#LINUX_USE_SWAP_FOR_RESUME="no"

# ENTRY_ROOT is a template that describes the beginning of the name for system-boot entries
# The ENTRY_ROOT will be followed by the kernel version number
# For example, if ENTRY_ROOT="manjaro" and you are using kernel 4.19 your entry will be named "manjaro4.19.conf"
#ENTRY_ROOT="arch"

# Use this pattern to match kernels which should be considered native OS kernels
#KERNEL_PATTERN="vmlinuz-linux*" \

# setting REMOVE_EXISTING to "yes" will remove all your existing systemd-boot entries before building new entries
#REMOVE_EXISTING="yes"

# unless OVERWRITE_EXISTING is set to "yes" existing entries for currently installed kernels will not be touched
# this setting has no meaning if REMOVE_EXISTING is set to "yes"
#OVERWRITE_EXISTING="no"

# when REMOVE_OBSOLETE is set to "yes" entries for kernels no longer available on the system will be removed
#REMOVE_OBSOLETE="yes"

# if PRESERVE_FOREIGN is set to "yes", do not delete entries starting with $ENTRY_ROOT
#PRESERVE_FOREIGN="no"

# setting NO_AUTOGEN to "yes" will stop the automatic creation of entries when kernels are installed
#NO_AUTOGEN="no"

# add discard option to cryptdevice parameters
#DISCARD="no"

# add discard option to boot parameters for filesystems (rootflags=discard) for continuous TRIM 
# see: https://wiki.archlinux.org/index.php/Solid_state_drive#Continuous_TRIM
#CDISCARD="no"
```

